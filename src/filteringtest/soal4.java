/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filteringtest;

import java.util.Arrays;
import java.util.Scanner;
/**
 *
 * @author User
 */
public class soal4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        minmax(scanner);
    }
    static void minmax(Scanner scanner){
        
        System.out.print(" Masukkan angka : ");
        String deret = scanner.nextLine();
        
        String[] array = deret.split(",");
        int[] intArray = convertArrayStringToArrayInt(array);
        
        Arrays.sort(intArray);

        int min = 0; 
        int max = 0; 
        for (int i = 0; i <  intArray.length; i++ ){
            int data = intArray[i];
            if (i < 4){
                min += data;
            }
        }
        for (int i = intArray.length-1; i > 0; i--){
            int data = intArray[i];
            if (i >= intArray.length - 4){
                max += data;
            }
        }
        System.out.println("Small = " + min);
        System.out.println("Big = " + max);
    }

    private static int[] convertArrayStringToArrayInt(String[] array) {
                int [] output = new int[array.length];

        for (int i = 0; i < array.length; i++){
            String data = array[i];

            int angka = Integer.parseInt(data);
            output[i] = angka;
        }
        return output;
    }
}
